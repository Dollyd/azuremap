/*
 * Copyright (C) 2012 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.example.mapdemo;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient.ConnectionCallbacks;
import com.google.android.gms.common.GooglePlayServicesClient.OnConnectionFailedListener;
import com.google.android.gms.location.LocationClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.InfoWindowAdapter;
import com.google.android.gms.maps.GoogleMap.OnInfoWindowClickListener;
import com.google.android.gms.maps.GoogleMap.OnMarkerClickListener;
import com.google.android.gms.maps.GoogleMap.OnMarkerDragListener;
import com.google.android.gms.maps.GoogleMap.OnMyLocationButtonClickListener;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.support.v4.app.FragmentActivity;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.View;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.view.animation.BounceInterpolator;
import android.view.animation.Interpolator;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;
import android.widget.Toast;

import java.io.InputStream;
import java.net.MalformedURLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.microsoft.windowsazure.mobileservices.*;

/**
 * This shows how to place markers on a map.
 */
public class MarkerDemoActivity extends FragmentActivity implements
		OnMarkerClickListener, OnInfoWindowClickListener, OnMarkerDragListener,
		OnSeekBarChangeListener, ConnectionCallbacks,
		OnConnectionFailedListener, LocationListener,
		OnMyLocationButtonClickListener {
	private static final LatLng BRISBANE = new LatLng(31.226431, 121.405296);
	private static final LatLng MELBOURNE = new LatLng(31.227796, 121.410057);
	private static final LatLng SYDNEY = new LatLng(31.228688, 121.410006);
	private static final LatLng ADELAIDE = new LatLng(31.230362, 121.408485);
	private static final LatLng PERTH = new LatLng(31.230603, 121.4033);

	private static final LatLng current = new LatLng(40.016276, 116.398825);

	private LocationClient mLocationClient;
	private boolean hasupdated;
	private Location currentLocation;
	private Handler mHandler;

	private MobileServiceClient mClient;
	private MobileServiceTable<TaxiList> mTaxiTable;
	private static final LocationRequest REQUEST = LocationRequest.create()
			.setInterval(5000) // 5 seconds
			.setFastestInterval(16) // 16ms = 60fps
			.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

	/** Demonstrates customizing the info window and/or its contents. */
	class CustomInfoWindowAdapter implements InfoWindowAdapter {
		private final RadioGroup mOptions;

		// These a both viewgroups containing an ImageView with id "badge" and
		// two TextViews with id
		// "title" and "snippet".
		private final View mWindow;
		private final View mContents;

		CustomInfoWindowAdapter() {
			mWindow = getLayoutInflater().inflate(R.layout.custom_info_window,
					null);
			mContents = getLayoutInflater().inflate(
					R.layout.custom_info_contents, null);
			mOptions = (RadioGroup) findViewById(R.id.custom_info_window_options);
		}

		@Override
		public View getInfoWindow(Marker marker) {
			if (mOptions.getCheckedRadioButtonId() != R.id.custom_info_window) {
				// This means that getInfoContents will be called.
				return null;
			}
			render(marker, mWindow);
			return mWindow;
		}

		@Override
		public View getInfoContents(Marker marker) {
			if (mOptions.getCheckedRadioButtonId() != R.id.custom_info_contents) {
				// This means that the default info contents will be used.
				return null;
			}
			render(marker, mContents);
			return mContents;
		}

		private void render(Marker marker, View view) {
			int badge;
			// Use the equals() method on a Marker to check for equals. Do not
			// use ==.
			if (marker.equals(mBrisbane)) {
				badge = R.drawable.badge_qld;
			} else if (marker.equals(startPoint)) {
				badge = R.drawable.badge_sa;
			} else if (marker.equals(mSydney)) {
				badge = R.drawable.badge_nsw;
			} else if (marker.equals(mMelbourne)) {
				badge = R.drawable.badge_victoria;
			} else if (marker.equals(mPerth)) {
				badge = R.drawable.badge_wa;
			} else {
				// Passing 0 to setImageResource will clear the image view.
				badge = 0;
			}
			((ImageView) view.findViewById(R.id.badge)).setImageResource(badge);

			String title = marker.getTitle();
			TextView titleUi = ((TextView) view.findViewById(R.id.title));
			if (title != null) {
				// Spannable string allows us to edit the formatting of the
				// text.
				SpannableString titleText = new SpannableString(title);
				titleText.setSpan(new ForegroundColorSpan(Color.RED), 0,
						titleText.length(), 0);
				titleUi.setText(titleText);
			} else {
				titleUi.setText("");
			}

			String snippet = marker.getSnippet();
			TextView snippetUi = ((TextView) view.findViewById(R.id.snippet));
			if (snippet != null && snippet.length() > 12) {
				SpannableString snippetText = new SpannableString(snippet);
				snippetText.setSpan(new ForegroundColorSpan(Color.MAGENTA), 0,
						10, 0);
				snippetText.setSpan(new ForegroundColorSpan(Color.BLUE), 12,
						snippet.length(), 0);
				snippetUi.setText(snippetText);
			} else {
				snippetUi.setText("");
			}
		}
	}

	private GoogleMap mMap;

	private Marker mPerth;
	private Marker mSydney;
	private Marker mBrisbane;
	private Marker mMelbourne;
	
	private Marker startPoint;// 初始位置marker
	private Marker endPoint;
	

	private final List<Marker> mMarkerRainbow = new ArrayList<Marker>();

	private TextView mTopText;
	private SeekBar mRotationBar;
	private CheckBox mFlatBox;

	private ImageButton searchBtn;
	private EditText edit;

	private final Random mRandom = new Random();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.marker_demo);
		mHandler = new Handler();
		hasupdated = false;
		mTopText = (TextView) findViewById(R.id.top_text);
		mRotationBar = (SeekBar) findViewById(R.id.rotationSeekBar);
		mRotationBar.setMax(360);
		mRotationBar.setOnSeekBarChangeListener(this);

		mFlatBox = (CheckBox) findViewById(R.id.flat);

		try {
			mClient = new MobileServiceClient(
					"https://mapdemo.azure-mobile.net/",
					"KTTiLjdzwLnWxskFWOaVtFLmHSlMlO34", this);
			mTaxiTable = mClient.getTable(TaxiList.class);
		TaxiList item = new TaxiList(); 
			item.lat = 40.017; 
			item.lng = 116.398; //自动触发服务器端insert, 
			mTaxiTable.insert(item, new
			TableOperationCallback<TaxiList>(){ 
			public void  onCompleted(TaxiList entity, Exception exception,
				ServiceFilterResponse response){ 
							 if (exception == null){ 
						 //insert succeeded
					  }else { // Insert failed 
						  createAndShowDialog(exception.getCause().getMessage(), "Error");
						 } 
					  } 
					 }); 
//			item.lat = 40.009; 
//			item.lng = 116.4001; 
//			mTaxiTable.insert(item, new
//					TableOperationCallback<TaxiList>(){ 
//					public void  onCompleted(TaxiList entity, Exception exception,
//						ServiceFilterResponse response){ 
//									 if (exception == null){ 
//								 //insert succeeded
//							  }else { // Insert failed 
//								  createAndShowDialog(exception.getCause().getMessage(), "Error");
//								 } 
//							  } 
//							 }); 
//			item.lat = 40.0188; 
//			item.lng = 116.40; 
//			mTaxiTable.insert(item, new
//					TableOperationCallback<TaxiList>(){ 
//					public void  onCompleted(TaxiList entity, Exception exception,
//						ServiceFilterResponse response){ 
//									 if (exception == null){ 
//								 //insert succeeded
//							  }else { // Insert failed 
//								  createAndShowDialog(exception.getCause().getMessage(), "Error");
//								 } 
//							  } 
//							 }); 
			
			/*
			 * //自动触发服务器端read mTaxiTable.parameter("opt",
			 * "search").parameter("curlat", "105") .parameter("curlng",
			 * "33").execute( new TableQueryCallback<TaxiList>() {
			 * 
			 * @Override public void onCompleted(List<TaxiList> result, int
			 * count, Exception exception, ServiceFilterResponse response) { if
			 * (exception == null) { for (TaxiList item : result) {
			 * Log.v("debug", item.getId());
			 * 
			 * } } else { } } });
			 */

		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		searchBtn = (ImageButton) findViewById(R.id.button1);
		searchBtn.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				InputMethodManager imm = (InputMethodManager) getApplicationContext()
						.getSystemService(Context.INPUT_METHOD_SERVICE);
				// 显示或者隐藏输入法
				imm.hideSoftInputFromInputMethod(edit.getWindowToken(), 0);
				String searchPlace = edit.getEditableText().toString().trim();
				if (searchPlace.equalsIgnoreCase("")) {
					Toast.makeText(getApplicationContext(),
							"Please enter where to go", Toast.LENGTH_SHORT)
							.show();
				} else {
					mMap.clear();
					searchTaxiFromTable();
					addMarkersToMap();
					getRouteString(searchPlace);
					
				}
			}
		});

		edit = (EditText) findViewById(R.id.editText1);

		setUpMapIfNeeded();

	}

	protected void createAndShowDialog(String message, String title) {
			AlertDialog.Builder builder = new AlertDialog.Builder(this);
			builder.setMessage(message);
			builder.setTitle(title);
			builder.create().show();
		
	}

	@Override
	protected void onResume() {
		super.onResume();
		setUpMapIfNeeded();
		setUpLocationClientIfNeeded();
		mLocationClient.connect();
		LocationManager mLocationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
		updateMyLocation(mLocationManager);

		// LatLngBounds.Builder builder = new LatLngBounds.Builder();
		// builder.include(current);
		// mMap.moveCamera(CameraUpdateFactory.newLatLngBounds(
		// builder.build(), 25, 25, 0));

	}

	private void setUpMapIfNeeded() {
		// Do a null check to confirm that we have not already instantiated the
		// map.
		if (mMap == null) {
			// Try to obtain the map from the SupportMapFragment.
			mMap = ((SupportMapFragment) getSupportFragmentManager()
					.findFragmentById(R.id.map)).getMap();

			// Check if we were successful in obtaining the map.
			if (mMap != null) {
				setUpMap();
				mMap.setMyLocationEnabled(true);
			}
		}
	}

	private void updateMyLocation(LocationManager mLocationManager) {
		if (mLocationManager != null) {
			Criteria criteria = new Criteria();
			criteria.setAccuracy(Criteria.ACCURACY_FINE);
			String prividerName = mLocationManager.getBestProvider(criteria,
					true);
			if (prividerName != null) {
				mLocationManager.requestLocationUpdates(prividerName,
						30l * 1000, 0.0f, mListener);
			}
		}
	}

	private void setUpMap() {
		// Hide the zoom controls as the button panel will cover it.
		mMap.getUiSettings().setZoomControlsEnabled(false);

		// Add lots of markers to the map.
		// addMarkersToMap();

		// Setting an info window adapter allows us to change the both the
		// contents and look of the
		// info window.
		mMap.setInfoWindowAdapter(new CustomInfoWindowAdapter());

		// Set listeners for marker events. See the bottom of this class for
		// their behavior.
		mMap.setOnMarkerClickListener(this);
		mMap.setOnInfoWindowClickListener(this);
		mMap.setOnMarkerDragListener(this);

		// Pan to see all markers in view.
		// Cannot zoom to bounds until the map has a size.
		final View mapView = getSupportFragmentManager().findFragmentById(
				R.id.map).getView();

		if (mapView.getViewTreeObserver().isAlive()) {
			// MapController myMapController = mapView.getController();
			mapView.getViewTreeObserver().addOnGlobalLayoutListener(
					new OnGlobalLayoutListener() {
						@SuppressWarnings("deprecation")
						// We use the new method when supported
						@SuppressLint("NewApi")
						// We check which build version we are using.
						@Override
						public void onGlobalLayout() {
							LatLngBounds bounds = new LatLngBounds.Builder()
									.include(PERTH).include(SYDNEY)
									.include(ADELAIDE).include(BRISBANE)
									.include(MELBOURNE).build();
							if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
								mapView.getViewTreeObserver()
										.removeGlobalOnLayoutListener(this);
							} else {
								mapView.getViewTreeObserver()
										.removeOnGlobalLayoutListener(this);
							}
							// mMap.moveCamera(CameraUpdateFactory
							// .newLatLngBounds(bounds, 50));
							mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(
									current, 14.0f));// 默认地图中心位置（current北京）
							startPoint = mMap.addMarker(new MarkerOptions()
									.position(current)
									.icon(BitmapDescriptorFactory
											.defaultMarker(BitmapDescriptorFactory.HUE_ORANGE)));
						}
					});
		}
	}

	private void addMarkersToMap() {
		clearMarkers();

		startPoint = mMap.addMarker(new MarkerOptions().position(current).icon(
				BitmapDescriptorFactory
						.defaultMarker(BitmapDescriptorFactory.HUE_ORANGE)));
	
		Circle circle;
		circle = mMap.addCircle(new CircleOptions()
				.center(new LatLng(40.016276, 116.398825)).radius(1000)
				.strokeWidth(2.0f).strokeColor(Color.TRANSPARENT)
				.fillColor(Color.HSVToColor(50, new float[] { 0f, 1f, 2f })));
		
		float rotation = mRotationBar.getProgress();
		boolean flat = mFlatBox.isChecked();
		
	}

	/** 找到当前位置500米内的出租车经纬度和距离 **/
	private void searchTaxiFromTable() {
		double curlat = current.latitude;// .getLatitude();
		double curlng = current.longitude;// getLongitude();
		Log.v("debug:current", current.toString());
		// Log.v("currentLocation", currentLocation.toString());
		// .parameter("curlat", "105")
		// .parameter("curlng", "33")
		mTaxiTable.parameter("opt", "search")
				.parameter("curlat", String.valueOf(curlat))
				.parameter("curlng", String.valueOf(curlng))
				.execute(new TableQueryCallback<TaxiList>() {
					@Override
					public void onCompleted(List<TaxiList> result, int count,
							Exception exception, ServiceFilterResponse response) {
						if (exception == null) {
							int i = 0;
							for (TaxiList item : result) {
								i++;
								Log.v("debug:search", item.toString());
								addResultMarker(item, i);
							}
						} else {
						}
					}
				});
	}

	/* 把搜索结果添加到地图上 */
	private void addResultMarker(TaxiList item, int i) {
		Marker place;
		place = mMap.addMarker(new MarkerOptions().position(item.getLatLng())
				.title(item.getDistance(current) + " Meters").snippet(item.toString()).draggable(false)
				.icon(BitmapDescriptorFactory.defaultMarker(i))
				.icon(getCustomizedIcon(String.valueOf(i))));

		Log.v("debug:marker", item.toString());
	}

	private boolean checkReady() {
		if (mMap == null) {
			Toast.makeText(this, R.string.map_not_ready, Toast.LENGTH_SHORT)
					.show();
			return false;
		}
		return true;
	}

	/** Called when the Clear button is clicked. */
	public void onClearMap(View view) {
		if (!checkReady()) {
			return;
		}
		mMap.clear();
	}

	/** Called when the Reset button is clicked. */
	public void onResetMap(View view) {
		if (!checkReady()) {
			return;
		}
		// Clear the map because we don't want duplicates of the markers.
		mMap.clear();
		addMarkersToMap();
	}

	/** Called when the Reset button is clicked. */
	public void onToggleFlat(View view) {
		if (!checkReady()) {
			return;
		}
		boolean flat = mFlatBox.isChecked();
		for (Marker marker : mMarkerRainbow) {
			marker.setFlat(flat);
		}
	}

	@Override
	public void onProgressChanged(SeekBar seekBar, int progress,
			boolean fromUser) {
		if (!checkReady()) {
			return;
		}
		float rotation = seekBar.getProgress();
		for (Marker marker : mMarkerRainbow) {
			marker.setRotation(rotation);
		}
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		if (mLocationClient != null) {
			mLocationClient.disconnect();
		}
	}

	@Override
	public void onStartTrackingTouch(SeekBar seekBar) {
		// Do nothing.
	}

	@Override
	public void onStopTrackingTouch(SeekBar seekBar) {
		// Do nothing.
	}

	//
	// Marker related listeners.
	//

	@Override
	public boolean onMarkerClick(final Marker marker) {
		// if (marker.equals(mPerth)) {
		// // This causes the marker at Perth to bounce into position when it
		// // is clicked.
		// final Handler handler = new Handler();
		// final long start = SystemClock.uptimeMillis();
		// final long duration = 1500;
		//
		// final Interpolator interpolator = new BounceInterpolator();
		//
		// handler.post(new Runnable() {
		// @Override
		// public void run() {
		// long elapsed = SystemClock.uptimeMillis() - start;
		// float t = Math.max(
		// 1 - interpolator.getInterpolation((float) elapsed
		// / duration), 0);
		// marker.setAnchor(0.5f, 1.0f + 2 * t);
		//
		// if (t > 0.0) {
		// // Post again 16ms later.
		// handler.postDelayed(this, 16);
		// }
		// }
		// });
		// } else if (marker.equals(mAdelaide)) {
		// // This causes the marker at Adelaide to change color and alpha.
		// marker.setIcon(BitmapDescriptorFactory.defaultMarker(mRandom
		// .nextFloat() * 360));
		// // marker.setAlpha(mRandom.nextFloat());
		// }
		// We return false to indicate that we have not consumed the event and
		// that we wish
		// for the default behavior to occur (which is for the camera to move
		// such that the
		// marker is centered and for the marker's info window to open, if it
		// has one).
		return false;
	}

	@Override
	public void onInfoWindowClick(Marker marker) {
//		Toast.makeText(getBaseContext(), "Click Info Window",
//				Toast.LENGTH_SHORT).show();
//		getRouteLatLng(marker.getPosition());
//		Log.w("route", "lat-->" + marker.getPosition().latitude + " longti-->"
//				+ marker.getPosition().longitude);
	}

	@Override
	public void onMarkerDragStart(Marker marker) {
		mTopText.setText("onMarkerDragStart");
	}

	@Override
	public void onMarkerDragEnd(Marker marker) {
		mTopText.setText("onMarkerDragEnd");
	}

	@Override
	public void onMarkerDrag(Marker marker) {
		mTopText.setText("onMarkerDrag.  Current Position: "
				+ marker.getPosition());
	}

	@Override
	public boolean onMyLocationButtonClick() {
		// TODO Auto-generated method stub
		Toast.makeText(this, "MyLocation button clicked", Toast.LENGTH_SHORT)
				.show();
		return false;
	}

	@Override
	public void onLocationChanged(Location arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onConnectionFailed(ConnectionResult arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onConnected(Bundle arg0) {
		// TODO Auto-generated method stub
		mLocationClient.requestLocationUpdates(REQUEST, this); // LocationListener
	}

	@Override
	public void onDisconnected() {
		// TODO Auto-generated method stub

	}

	private void setUpLocationClientIfNeeded() {
		if (mLocationClient == null) {
			mLocationClient = new LocationClient(getApplicationContext(), this, // ConnectionCallbacks
					this); // OnConnectionFailedListener
		}
	}

	/**
	 * Button to get current Location. This demonstrates how to get the current
	 * Location as required without needing to register a LocationListener.
	 */
	public void showMyLocation(View view) {
		if (mLocationClient != null && mLocationClient.isConnected()) {
			String msg = "Location = " + mLocationClient.getLastLocation();
			Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_SHORT)
					.show();
		}
	}

	private android.location.LocationListener mListener = new android.location.LocationListener() {

		@Override
		public void onStatusChanged(String provider, int status, Bundle extras) {
			// TODO Auto-generated method stub

		}

		@Override
		public void onProviderEnabled(String provider) {
			// TODO Auto-generated method stub

		}

		@Override
		public void onProviderDisabled(String provider) {
			// TODO Auto-generated method stub

		}

		@Override
		public void onLocationChanged(Location location) {
			currentLocation = location;
			// Log.v("current location", currentLocation.toString());

			if (mMap != null) {
				// LatLng current = new LatLng(40.016276,116.398825);
				//
				// current
				// mMap.addMarker(new MarkerOptions().position(new
				// LatLng(location
				// .getLatitude(), location.getLongitude())));
				if (!hasupdated) {
					Log.v("current location", "listener");
					// mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(
					// current, 16));//有效！地图显示跳到current视图（北京）
					hasupdated = true;
				}
			}
		}
	};

	private LatLng getRandomLatLngNearCurrent() {
		return new LatLng(currentLocation.getLatitude()
				+ (mRandom.nextDouble() * 0.02 - 0.01),
				currentLocation.getLongitude()
						+ (mRandom.nextDouble() * 0.02 - 0.01));
	}

	private void clearMarkers() {
		if (mPerth != null)
			mPerth.remove();
		if (mSydney != null)
			mSydney.remove();
		if (mBrisbane != null)
			mBrisbane.remove();
		if (startPoint != null)
			startPoint.remove();
		if (mMelbourne != null)
			mMelbourne.remove();
	}

	private BitmapDescriptor getCustomizedIcon(String number) {
		Bitmap.Config conf = Bitmap.Config.ARGB_8888;
		Bitmap bmp = Bitmap.createBitmap(100, 100, conf);
		Canvas canvas = new Canvas(bmp);

		Paint paint = new Paint();
		paint.setColor(Color.WHITE);
		paint.setTextSize(25);
		paint.setStrokeWidth(2.0f);
		paint.setStyle(Style.STROKE);

		Bitmap b = BitmapFactory.decodeResource(getResources(),
				R.drawable.icon_blue);// 设置图标
		canvas.drawBitmap(Bitmap.createScaledBitmap(b, 100, 100, false), 0, 0,
				paint);// new BitmapDrawable(res)
		canvas.drawText(number, 43, 41, paint);// 设置图标上的字（内容、位置、颜色）
		return BitmapDescriptorFactory.fromBitmap(bmp);
	}

	public Document getDocument(LatLng start, String destination, String mode) {
		String url = "http://maps.googleapis.com/maps/api/directions/xml?"
				+ "origin=" + start.latitude + "," + start.longitude
				+ "&destination=" + destination
				+ "&sensor=false&units=metric&mode=driving";
		Log.d("url", url);
		try {
			HttpClient httpClient = new DefaultHttpClient();
			HttpContext localContext = new BasicHttpContext();
			HttpPost httpPost = new HttpPost(url);
			HttpResponse response = httpClient.execute(httpPost, localContext);
			InputStream in = response.getEntity().getContent();
			DocumentBuilder builder = DocumentBuilderFactory.newInstance()
					.newDocumentBuilder();
			Document doc = builder.parse(in);
			return doc;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	public Document getDocument2(LatLng start, LatLng end, String mode) {
		String url = "http://maps.googleapis.com/maps/api/directions/xml?"
				+ "origin=" + start.latitude + "," + start.longitude
				+ "&destination=" + end.latitude + "," + end.longitude
				+ "&sensor=false&units=metric&mode=driving";
		
		Log.d("url", url);
		try {
			HttpClient httpClient = new DefaultHttpClient();
			HttpContext localContext = new BasicHttpContext();
			HttpPost httpPost = new HttpPost(url);
			HttpResponse response = httpClient.execute(httpPost, localContext);
			InputStream in = response.getEntity().getContent();
			DocumentBuilder builder = DocumentBuilderFactory.newInstance()
					.newDocumentBuilder();
			Document doc = builder.parse(in);
			return doc;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	public ArrayList<LatLng> getDirection(Document doc) {
		NodeList nl1, nl2, nl3;
		ArrayList<LatLng> listGeopoints = new ArrayList<LatLng>();
		nl1 = doc.getElementsByTagName("step");
		if (nl1.getLength() > 0) {
			for (int i = 0; i < nl1.getLength(); i++) {
				Node node1 = nl1.item(i);
				nl2 = node1.getChildNodes();

				Node locationNode = nl2
						.item(getNodeIndex(nl2, "start_location"));
				nl3 = locationNode.getChildNodes();
				Node latNode = nl3.item(getNodeIndex(nl3, "lat"));
				double lat = Double.parseDouble(latNode.getTextContent());
				Node lngNode = nl3.item(getNodeIndex(nl3, "lng"));
				double lng = Double.parseDouble(lngNode.getTextContent());
				listGeopoints.add(new LatLng(lat, lng));

				locationNode = nl2.item(getNodeIndex(nl2, "polyline"));
				nl3 = locationNode.getChildNodes();
				latNode = nl3.item(getNodeIndex(nl3, "points"));
				ArrayList<LatLng> arr = decodePoly(latNode.getTextContent());
				for (int j = 0; j < arr.size(); j++) {
					listGeopoints.add(new LatLng(arr.get(j).latitude, arr
							.get(j).longitude));
				}

				locationNode = nl2.item(getNodeIndex(nl2, "end_location"));
				nl3 = locationNode.getChildNodes();
				latNode = nl3.item(getNodeIndex(nl3, "lat"));
				lat = Double.parseDouble(latNode.getTextContent());
				lngNode = nl3.item(getNodeIndex(nl3, "lng"));
				lng = Double.parseDouble(lngNode.getTextContent());
				listGeopoints.add(new LatLng(lat, lng));
			}
		}

		return listGeopoints;
	}

	private int getNodeIndex(NodeList nl, String nodename) {
		for (int i = 0; i < nl.getLength(); i++) {
			if (nl.item(i).getNodeName().equals(nodename))
				return i;
		}
		return -1;
	}

	private ArrayList<LatLng> decodePoly(String encoded) {
		ArrayList<LatLng> poly = new ArrayList<LatLng>();
		int index = 0, len = encoded.length();
		int lat = 0, lng = 0;
		while (index < len) {
			int b, shift = 0, result = 0;
			do {
				b = encoded.charAt(index++) - 63;
				result |= (b & 0x1f) << shift;
				shift += 5;
			} while (b >= 0x20);
			int dlat = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
			lat += dlat;
			shift = 0;
			result = 0;
			do {
				b = encoded.charAt(index++) - 63;
				result |= (b & 0x1f) << shift;
				shift += 5;
			} while (b >= 0x20);
			int dlng = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
			lng += dlng;

			LatLng position = new LatLng((double) lat / 1E5, (double) lng / 1E5);
			poly.add(position);
		}
		return poly;
	}

	public static ArrayList<LatLng> googleReturnPoints(LatLng start,LatLng end) {
		
		String url = "http://maps.googleapis.com/maps/api/directions/xml?origin="
				+ start.latitude
				+ ","
				+ start.longitude
				+ "&destination="
				+ end.latitude
				+ ","
				+ "&sensor=false&units=metric";
		// "http://ditu.google.cn/maps/api/directions/xml?origin="+lat1+ "," +
		// lon1 +"&destination="+lat2 + "," + lon2 +"&sensor=true";
		String tag[] = { "lat", "lng" };
		ArrayList<LatLng> list_of_geopoints = new ArrayList<LatLng>();
		HttpResponse response = null;
		try {
			HttpClient httpClient = new DefaultHttpClient();
			HttpContext localContext = new BasicHttpContext();
			HttpPost httpPost = new HttpPost(url);
			response = httpClient.execute(httpPost, localContext);
			InputStream in = response.getEntity().getContent();
			DocumentBuilder builder = DocumentBuilderFactory.newInstance()
					.newDocumentBuilder();
			Document doc = builder.parse(in);
			if (doc != null) {
				NodeList nl1, nl2;
				nl1 = doc.getElementsByTagName(tag[0]);
				nl2 = doc.getElementsByTagName(tag[1]);
				if (nl1.getLength() > 0) {
					// list_of_geopoints = new ArrayList();
					for (int i = 0; i < nl1.getLength(); i++) {
						Node node1 = nl1.item(i);
						Node node2 = nl2.item(i);
						double lat = Double.parseDouble(node1.getTextContent());
						double lng = Double.parseDouble(node2.getTextContent());
						list_of_geopoints.add(new LatLng(lat, lng));
						// new GeoPoint((int) (lat * 1E6), (int) (lng * 1E6)));
					}
				} else {
					// No points found
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list_of_geopoints;
	}
	protected void getRouteLatLng(final LatLng position) {
		new Thread(new Runnable() {

			@Override
			public void run() {
				// TODO Auto-generated method stub
				// md = new GMapV2Direction();
				
				Document doc = getDocument2(current, position, "walking");
				ArrayList<LatLng> directionPoint = getDirection(doc);
				final PolylineOptions rectLine = new PolylineOptions().width(7)
						.color(Color.RED);

				for (int i = 0; i < directionPoint.size(); i++) {
					rectLine.add(directionPoint.get(i));
					Log.w("polyline", directionPoint.get(i).latitude + " "
							+ directionPoint.get(i).longitude);
				}
				updatePolyline(rectLine);

			}

			private void updatePolyline(final PolylineOptions rectLine) {
				mHandler.post(new Runnable() {

					@Override
					public void run() {
						// TODO Auto-generated method stub
						Polyline polylin = mMap.addPolyline(rectLine);
					}
				});
			}
		}).start();

	}

	protected void getRouteString(final String searchPlace) {
		new Thread(new Runnable() {

			@Override
			public void run() {
				Document doc = getDocument(current, searchPlace, "driving");
				final ArrayList<LatLng> directionPoint = getDirection(doc);
				final PolylineOptions rectLine = new PolylineOptions().width(7)
						.color(Color.RED);
				
				for (int i = 0; i < directionPoint.size(); i++) {
					rectLine.add(directionPoint.get(i));
					
					Log.w("polyline", directionPoint.get(i).latitude + " "
							+ directionPoint.get(i).longitude);
				}
				mHandler.post(new Runnable() {
					//对界面操作要回到主线程
					@Override
					public void run() {
						endPoint = mMap.addMarker(new MarkerOptions().position(directionPoint.get(directionPoint.size()-1)).icon(
								BitmapDescriptorFactory
										.defaultMarker(BitmapDescriptorFactory.HUE_VIOLET)));
					}
				});
				updatePolyline(rectLine);

			}
			private void updatePolyline(final PolylineOptions rectLine) {
				mHandler.post(new Runnable() {
					@Override
					public void run() {
						Polyline polylin = mMap.addPolyline(rectLine);
					}
				});
			}
		}).start();

	}
}
