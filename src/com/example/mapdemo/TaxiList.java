package com.example.mapdemo;

import java.util.Date;

import com.google.android.gms.maps.model.LatLng;

public class TaxiList{
	public String Id;
	public Date time;
	public double lat;
	public double lng;
	private static final double EARTH_RADIUS = 6378137;
	
	private static double rad(double d)  
    {  
       return d * Math.PI / 180.0;  
    }  
	
	public String getId() {
		// TODO Auto-generated method stub
		return Id;
	}
	
	public double getLat() {
		// TODO Auto-generated method stub
		return lat;
	}
	
	public double getLng() {
		// TODO Auto-generated method stub
		return lng;
	}
	
	public LatLng getLatLng(){
		return new LatLng(lat,lng);
	}
	
	public String toString(){
		return "( "+String.valueOf(lat)+","+String.valueOf(lng)+" )";
	}

	public double getDistance(LatLng current) {
		double radLat1 = rad(current.latitude);
	     double radLat2 = rad(lat);
	     double a = radLat1 - radLat2;
	     double b = rad(current.longitude) - rad(lng);
	     double s = 2 * Math.asin(Math.sqrt(Math.pow(Math.sin(a/2),2) +   
	             Math.cos(radLat1)*Math.cos(radLat2)*Math.pow(Math.sin(b/2),2)));  
	            s = s * EARTH_RADIUS;  
	            s = Math.round(s * 10000) / 10000;  
	     return s;
		
	}
}
